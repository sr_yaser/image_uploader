Hi Yaser,

I think the best place to start is giving you a run down of the system and its complexity. Unfortunately we don't have documentation of the system to follow so I will try to fill all gaps.
The system has the following entry points:

Public website - https://estatemate-dev.co.za
Resident Portal (Auth Entry) - https://resident.estatemate-dev.co.za
Agent Portal - https://agent.estatemate-dev.co.za
Admin Portal - https://admin.estatemate-dev.co.za

Admin portal is for EstateMate (EM) administrators, where they create clients, client personnel, estates, properties, users, service providers, security profiles.

Clients are property management companies and are in charge of overseeing the estates (residential communities). They have users which are called "Agents", and agents make use of the "Agent portal". When the agents login they can see a dashboard where they have the tools to communicate and manage the residential communities. They have a connection to the resident ticket items, communication, and the ability to create an estate and add users etc.

Within a residential community there are selected individuals who have the responsibility to look after their own estate and its residents. They therefore have a promoted user rights when compared to other residents, but they don't have access to tools to create their own estate. These people are called managers.

Managers and Agents make up the entity 'Management' and them together manage the communication and responsibility of a single estate.

Residents and managers access the residential portal, and log and access a bunch of features. The residents can do things like contact security, add additional users, report an issue, access service providers to fix things, request approval from management to do something, live chat to the managers in the community, respond to discussion forums, voting items etc. When managers log in they have exactly the same features but also a few additional features, like looking at outstanding approval items, or reports, or requests to use the resident app, to create a notification etc.
I will provide you the login credentials for each area:

Admin:
admin@estatemate.co
test123

Agent:
terry.shoreline@yopmail.com
word2pass

Manager:
jens.lavender@yopmail.com
word2pass

Resident:
roy.lavender@yopmail.com
word2pass
Agent + Managers = Management
Any communication from the resident goes to management, and management can action on that communication.
Every action in the system must have a reaction. E.g. If a report is sent to management, then management must be notification via push notification and email, and resident must get confirmation of what has been sent.
So your first task is to review the system as a whole from a user perspective. Once you have setup your dev environment locally, then do the following:
So I want you to do the following:

Admin
1. Create a client, and for that client create an agent(client personnel)
2. Create a security company profile
3. Create a service provider in admin
4. Create a new estate, then add your agent, security and service provider
5. Create a property for the estate, then add a user for that property, send an OTP to the new user.
6. Add additional documents to the estate, and additional contacts

Go to resident.estatemate-dev.co.za
1. Register as a user using OTP, see how it fetches user information.
2. Register without OTP and see how this requires management approval before being able to login


Go back to admin.estatemate-dev.co.za and change your original user to manager in the users tab in the estate detail page. You have now promoted the user to 'manager' level. Now login with this user in resident.estatemate-dev.co.za and see what you have as options. You should have a notification that requests you to respond to a user registraion request. This is for the user that you registered without OTP.
Basically I just need you to gain a good understanding of the broader system as a whole, and how we create these environments for communication.
9:53 PM
Once you have done all this, then I will task you further. But I will mention to you what you first task is so that while you go through the system you can start planning how you want to develop this. I need you to develop a way to automatically create a custom URL/web link. This custom URL will be for residents to login to the resident portal through a web page that has the 'estate' or 'client' logo instead of the standard EstateMate logo.

Example:

There is an estate called "Blue Valley Estate", and now they can use the login site found at "resident.estatemate-dev.co.za", but they want their custom URL so that they can add it to their website as a link so that when their resident goes to the website, and clicks on the login page it redirects to the resident portal which has a few icons and logo which look like Blue Valley, and not estatemate. It needs to be only super simple. My thinking is the following:

For client urls:

1. Client called "Jacks Properties" is registered on the system, and this creates automatically a custom URL like "jacks-properties.estatemate-dev.co.za" or "resident.estatemate-dev.co.za/jacksproperties".
2. If this url is used then it displays a login screen for the estates for Jacks Properties ONLY. This is where the user can login or register. The only difference being that the EstateMate logo is replaced by the 'Client Logo" which is Jacke Properties logo.

For estates custom login:

1. Estate called "Blue Valley" is created on the system, and this creates automatically a custom URL like "bluevalley.estatemate-dev.co.za" or "resident.estatemate-dev.co.za/bluevalley".
2. If this url is used then it displays a login screen for the properties for Blue Valley ONLY. This is where the user can login or register. The only difference being that the EstateMate logo is replaced by the 'Estate Logo" which is Blue Valley's logo.

You will see that the logo's are requested when creating a client or creating an estate.
Jens Hiestermann, 01/02/2018
