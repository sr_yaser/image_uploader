<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Uploader extends Controller {

    public function action_index() {
        $imageObj = ORM::factory('Images');
	$images = $imageObj->find_all();
        $imageList = array();
        foreach ($images as $image) {
            $imageList[] = [
                'id' => $image->id,
                'title' => $image->title,
                'image_file' => $image->image_file,
                'created_at' => $image->created_at,
            ];
        }
        $view = new View('uploader_view');
        $view->set("images", $imageList);
        $this->response->body($view);
    }

    public function action_upload() {
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');

        $imageFile = Validation::factory($_FILES);
        $image = $imageFile['image'];
        $filename = 'noimage';

        if (Upload::not_empty($image) && Upload::valid($image) && Upload::type($image, array('png', 'jpeg', 'jpg'))) {
            $path = DOCROOT . 'uploads/';
            $filename = Upload::save($image);
            if (!$filename) {
                throw new Exception('Unable to upload image');
            }
            $filename = basename($filename);
        } else {
            throw new Exception('Invalid image');
        }

        $imageModel = new Model_Images();
        $imageModel->image_file = $filename;
        $imageModel->title = $this->request->post('title');
        $imageModel->created_at = date('Y-m-d H:i:s');
        $imageModel->save();

        $imageData = [
            'id' => $imageModel->id,
            'title' => $imageModel->title,
            'filename' => $imageModel->image_file,
            'created_date' => $imageModel->created_at,
        ];

        return $this->response->body(
            json_encode(
                [
                    'data' => $imageData
                ]
            )
        );
    }
    
    public function action_edit(){
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
        $id = $this->request->post('edit_id');
        
        $imageFile = Validation::factory($_FILES);
        $image = $imageFile['image_edit'];
        $filename = null;
        
        if (Upload::not_empty($image) && Upload::valid($image) && Upload::type($image, array('png', 'jpeg', 'jpg'))) {
            $path = DOCROOT . 'uploads/';
            $filename = Upload::save($image);
            $filename = basename($filename);
        }
        
        $imageModel = ORM::factory('Images');
	$image = $imageModel->where('id', '=', $id)->find();
        $image->title = $this->request->post('edit_title');
        if($filename != null)
          $image->image_file = $filename;
        $image->updated_at = date('Y-m-d H:i:s');
        
        $image->save();
        return $this->response->body(
            json_encode(
                [
                    'data' => $image
                ]
            )
        );
    }
    
    public function action_delete(){
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
        $id = Arr::get($_GET, 'id', '0');
        
        $imageModel = ORM::factory('Images');
	$image = $imageModel->where('id', '=', $id)->find();
        unlink('upload/'. $image->image_file);
	$image->delete();

	if (! $image) {
            echo "no image";
	}else{
            echo "image name: ".$image->title;
        }
    }
    
    public function action_getimage(){
        $this->response->headers('Content-Type', 'application/json; charset=utf-8');
        $id = Arr::get($_GET, 'id', '0');
        
        $imageModel = ORM::factory('Images');
	$image = $imageModel->where('id', '=', $id)->find();
        
        $imageData = [
            'id' => $image->id,
            'title' => $image->title,
            'filename' => $image->image_file,
            'created_date' => $image->created_at,
        ];

        return $this->response->body(
            json_encode(
                [
                    'data' => $imageData
                ]
            )
        );
    }

}
