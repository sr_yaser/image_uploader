<?php

class Model_Images extends ORM {
    
    protected $_table_name = 'images';
    
    protected $_table_columns = array(
	'id' => array(),
        'title' => array(),
	'image_file' => array(),
	'created_at' => array(),
	'updated_at' => array()
    );
}
