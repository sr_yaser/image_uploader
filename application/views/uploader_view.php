<!-- Trigger/Open The Modal -->
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
        </script>
        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/style.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="/assets/css/modal-css.css" />
    </head>
    <!-- The Modal -->
    <h3 style="width: 100%; text-align: center;">Image Uploader</h3>
    <div>
        <table border="1" id="image_table">	
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Filename</th>
                <th>Date Added</th>
                <th>Actions</th>
            </tr>

            <tbody id="table_content">

            </tbody>

        </table>
    </div>

    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <h4>Upload new Image</h4>
            <form id="uploader_form" action="uploader/upload" method="post" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td><label for="title">Title:</label></td>
                        <td><input type="text" name="title" id="title" /><br></td>
                    </tr>
                    <tr>
                        <td><label for="image">Image:</label></td>
                        <td><input type="file" name="image" id="image" /></td>
                    </tr>
                    <tr>
                        <td>
                            <div id="image_thumb_id_1">
                                <img id='image_preview_1' alt="No image" style='height: 50px' width="50" src=''/>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <br>
                
                <div class="wrapper">
                    <input class="submit_btn" type="submit" value="Upload" />
                    <input class="cancel_btn" onclick="closeModal()" type="reset" value="Cancel" />
                </div>
                
            </form>
        </div>

    </div>

    <div id="myModal_edit" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="edit_close" style="margin-left: 345px;">&times;</span>
            <h4>Edit this Image</h4>
                <form id="uploader_edit_form" action="uploader/edit" method="post" enctype="multipart/form-data">
                <input type="hidden" name="edit_id" id="edit_id" />
                <table>
                    <tr>
                        <td><label for="title">Title:</label></td>
                        <td><input type="text" name="edit_title" id="edit_title" /></td>
                    </tr>
                    <tr>
                        <td><label for="image">Image:</label></td>
                        <td><input type="file" name="image_edit" id="image_edit" /></td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                                <div id="image_thumb_id">
                                </div>
                            </div>
                        </td>
                        <td></td>
                    </tr>
                </table>
                <div class="wrapper">
                    <input class="submit_btn" type="submit" value="Upload" />
                    <input class="cancel_btn" onclick="closeEditModal()" type="reset" value="Cancel" />
                </div>
                </form>
        </div>
    </div>
    
    <div id="image_modal" class="modal">

        <!-- Modal content -->
        <div class="modal-content" style="width: 50%; height: auto">
            <span id="image_modal_close_span" class="close">&times;</span>
            <img id="image_display_id" style="max-width: 100%; height: auto" alt="Image view"/>
        </div>

    </div>

    <br>
    <div class="wrapper">
        <button class="add_btn" id="myBtn">Add new Image</button>
    </div>

    <script>

        $(function () {
<?php if (!empty($images)): ?>
    <?php foreach ($images as $image): ?>
                    var imageObj = {
                        id: '<?php echo $image['id']; ?>',
                        title: '<?php echo $image['title']; ?>',
                        filename: '<?php echo $image['image_file']; ?>',
                        created_date: '<?php echo $image['created_at']; ?>',
                    };
                    var markup = "<tr id=" + imageObj.id + ">\n\
                            <td>" + imageObj.title + "</td>\n\
                            <td><img style='height: 50px' src='upload/"+imageObj.filename +"' onclick='showImage(`"+imageObj.filename+"`)' /></td>\n\
                            <td>" + imageObj.filename + "</td>\n\
                            <td>" + imageObj.created_date + "</td>\n\
                            <td><input class='myButton' type='button' value='Delete' onclick='deleteImage(this," + imageObj.id + ")'/>\n\
                            <input class='myButton' type='button' value='Edit' onclick='editImage(this, " + imageObj.id + ")'/></td>\n\
                            </tr>";

                    $("#table_content").append(markup);
                    console.log(imageObj);
    <?php endforeach; ?>
<?php endif; ?>
        });


        var modal = document.getElementById('myModal');
        var btn = document.getElementById("myBtn");
        var span = document.getElementsByClassName("close")[0];
        btn.onclick = function () {
            modal.style.display = "block";
            $('#image_preview_1').hide();
        }
        span.onclick = function () {
            modal.style.display = "none";
        }
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
        var i = 0;

        $('#uploader_form').on('submit', (function (e) {
            e.preventDefault();
            var url = "uploader/upload";
            var uploaderForm = new FormData(this);
            $.ajax({
                url: url,
                type: "POST",
                data: uploaderForm,
                contentType: false,
                processData: false,
                success: function (response) {
                    var image = response.data;
                    addTableRow(image);
                    $('#uploader_form')[0].reset();
                    $('#myModal').hide();
                },
                error: function (xhr, status, data) {
                    console.log("Error");
                }
            });
            return false;
        }));
        
        $('#uploader_edit_form').on('submit', (function (e) {
            e.preventDefault();
            var url = "uploader/edit";
            $.ajax({
                url: url,
                type: "POST",
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function (response) {
                    var image = response.data;
//                    addTableRow(image);
                    window.location.reload();
                    $('#myModal_edit').hide();
                },
                error: function (xhr, status, data) {
                    console.log("Error");
                }
            });
            return false;
        }));

        function addTableRow(image) {
            var id = 'file_' + i;
            var markup = "<tr id=" + image.id + ">\n\
                    <td>" + image.title + "</td>\n\
                    <td><img style='height: 50px' src='upload/" + image.filename + "' onclick='showImage(`"+image.filename+"`)'/></td>\n\
                    <td>" + image.filename + "</td>\n\
                    <td>" + image.created_date + "</td>\n\
                    <td><input class='myButton' type='button' value='Delete' onclick='deleteImage(this," + image.id + ")'/>\n\
                    <input class='myButton' type='button' value='Edit' onclick='editImage(this, " + image.id + ")'/></td>\n\
                    </tr>";
            $("#table_content").append(markup);
        }

        function deleteImage(btn, imageId) {
            var url = "uploader/delete?id=" + imageId;
            $.ajax({
                url: url,
                type: "GET",
                contentType: false,
                processData: false,
                success: function (response) {
                    console.log("success");
                },
                error: function (xhr, status, data) {
                    console.log("Error");
                }
            });
            var row = btn.parentNode.parentNode;
            row.parentNode.removeChild(row);
        }

        function editImage(btn, image_id) {
            var row = btn.parentNode.parentNode;
            var url = "uploader/getimage?id=" + image_id;
            $.ajax({
                url: url,
                type: "GET",
                contentType: false,
                processData: false,
                success: function (response) {
                    var image = response.data;
                    var editModal = document.getElementById('myModal_edit');
                    var span = document.getElementsByClassName("edit_close")[0];
                    editModal.style.display = "block";
                    span.onclick = function () {
                        editModal.style.display = "none";
                    }
                    window.onclick = function (event) {
                        if (event.target == editModal) {
                            editModal.style.display = "none";
                        }
                    }
                    $('#edit_title').val(image.title);
                    $('#edit_id').val(image.id);
                    $('#image_thumb_id').html("<img id='image_preview' style='height: 50px' src='upload/" + image.filename + "'/>");
                },
                error: function (xhr, status, data) {
                    console.log("Error");
                }
            });
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        function readURL_1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image_preview_1').attr('src', e.target.result);
                    $('#image_preview_1').show();
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#image_edit").change(function () {
            readURL(this);
        });
        
        $("#image").change(function () {
            readURL_1(this);
        });
        
        function closeModal(){
            var modal = document.getElementById('myModal');
            $('#uploader_form')[0].reset();
            modal.style.display = "none";
        }
        function closeEditModal(){
            var modal = document.getElementById('myModal_edit');
            modal.style.display = "none";
        }
        
        function showImage(image){
            $('#image_display_id').attr('src', 'upload/'+image);
            var image_modal = document.getElementById('image_modal');
            image_modal.style.display = "block";
            $('#image_modal_close_span').click(function(e){
                image_modal.style.display = "none";
            });
        }

    </script>

</html>