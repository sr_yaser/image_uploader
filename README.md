# Image Uploader: A Test project in Kohana framework


## Installation

- Create a new virtual host on your server.
- Clone this repository to the webroot of this new virtual host.	
- Create a new mysql database and import the DB schema located at `db/image_db.sql`.
- Go to `modules/database/config/database.php`, and modify database username, password and db name.

NOTE: The app is designed to work from the document root. If you put it inside a subfolder it might not work.

## User Documentation

- Open a web browser and enter the url:`http://localhost/image_uploader`. You may need to modify the hostname based on your virtual host settings

- Appplication shows the images in a table

- New images can be uploaded by clicking 'Add new Image' button.

- Existing images can be edited and deleted by clicking corresponding buttons on action column.